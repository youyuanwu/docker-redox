FROM debian:buster-slim

RUN apt-get update && apt-get -y upgrade && \
    apt-get -y install \
        qemu \
        qemu-kvm \
        udhcpd \
        wget \
        alsa-utils \
        sudo \
    && apt-get clean

SHELL [ "/bin/bash", "-c" ]

# Hope that this is accidental that new user has the same user id as host user for x11 to work
RUN useradd redoxuser -p redoxuser \
    && tee -a /etc/sudoers <<< 'redoxuser ALL=(ALL) NOPASSWD: ALL' \
    && mkdir /home/redoxuser \
    && mkdir /home/redoxuser/redox \
    && chown -R redoxuser:redoxuser /home/redoxuser

USER redoxuser
    
WORKDIR /home/redoxuser/redox

# Get image for redox
RUN wget --no-check-certificate https://gitlab.redox-os.org/redox-os/redox/-/jobs/31100/artifacts/raw/build/img/redox_0.6.0_harddrive.bin.gz
RUN gzip -d redox_0.6.0_harddrive.bin.gz
ENV IMAGE_PATH /home/redoxuser/redox/redox_0.6.0_harddrive.bin

ENV DISPLAY=:0.0

VOLUME [ "/tmp/.X11-unix" ]

RUN touch Launch.sh \
    && chmod +x ./Launch.sh \
    && tee -a Launch.sh <<< '#!/bin/bash' \
    && tee -a Launch.sh <<< 'set -eux' \
    && tee -a Launch.sh <<< 'sudo chown    $(id -u):$(id -g) /dev/kvm 2>/dev/null || true' \
    && tee -a Launch.sh <<< 'sudo chown -R $(id -u):$(id -g) /dev/snd 2>/dev/null || true' \
    && tee -a Launch.sh <<< 'exec qemu-system-x86_64 -serial mon:stdio \' \
    && tee -a Launch.sh <<< '-d cpu_reset -d guest_errors \' \
    && tee -a Launch.sh <<< '-smp 4 -m 1024 -s -machine q35 \' \
    && tee -a Launch.sh <<< '-device ich9-intel-hda -device hda-duplex \' \
    && tee -a Launch.sh <<< '-net nic,model=e1000 -net user \' \
    && tee -a Launch.sh <<< '-device nec-usb-xhci,id=xhci -device usb-tablet,bus=xhci.0 \' \
    && tee -a Launch.sh <<< '-enable-kvm -cpu host \' \
    && tee -a Launch.sh <<< '-drive file=${IMAGE_PATH},format=raw'

CMD /bin/bash -c ./Launch.sh