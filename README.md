# docker-redox

Run redox in docker with qemu. Aim to let user try out redox as easily as possible.

# Prerequisite
* Host system must be linux
* Requied packages on host system:
    * docker
    * qemu

# Quick Start
`make build` builds the docker image
`make run` launch redox in a docker container. qemu UI should be visible.

# Implementation
* docker engine runs a linux container, inside which qemu is launched with redox harddisk image. 
* qemu UI is forwarded via X11 to the host system.
* Inspired by [docker-OSX](https://github.com/sickcodes/Docker-OSX)