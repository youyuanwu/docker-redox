build:
	docker build -t redox:dev -f Dockerfile .

DISPLAY ?=:0.0
run:
	docker run -it --rm \
		--name redox-container \
		--device /dev/kvm \
		-v /tmp/.X11-unix:/tmp/.X11-unix \
		-e "DISPLAY=${DISPLAY}" \
		redox:dev